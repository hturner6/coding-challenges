var player;
var platforms = [];
var ground;
function setup() {
	createCanvas(1000,600);
	player = new Player(100, 100);
	ground = new Platform(true);
	platforms.push(ground);
}


function draw() {
	background(51);
	
	if (frameCount % 65 == 0) {
		platforms.push(new Platform());
	}

	if (keyIsDown(LEFT_ARROW)) {
		player.move(8);
	} else if (keyIsDown(RIGHT_ARROW)) {
		player.move(-8);
	}
	player.show();
	player.update(platforms);
	for (var i = 0; i < platforms.length; i++) {
		platforms[i].show();
		platforms[i].update();
		if (platforms[i].offScreen()) {
			platforms.splice(i, 1);
		}
	}

}

function keyPressed() {
	if (keyCode == ' ' || keyCode == UP_ARROW) {
		for (var i = 0; i < platforms.length; i++) {
			if (player.collides(platforms[i])) {
				player.jump(16);
			}
		}
	}
}
