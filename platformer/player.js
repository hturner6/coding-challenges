class Player {
	constructor(x_, y_) {
		this.pos = createVector(x_, y_);
		this.width = 36;
		this.height = 72;
		this.gravity = createVector(0, 0.75);
		this.velocity = createVector(0, 0);
	}

	show() {
		fill(255);
		rect(this.pos.x, this.pos.y, this.width, this.height);
	}

	update(platforms) {
		//Check if player is colliding with a platform
		this.velocity.add(this.gravity);
		this.pos.add(this.velocity);
		for (var i = 0; i < platforms.length; i++) { 
			if (this.collides(platforms[i])) {
				this.velocity.y = 0;
				//TODO: Attempt to make player move with platform
				this.velocity.x += platforms[i].velocity; 
				console.log(platforms[i].velocity);
				this.pos.y = platforms[i].pos.y-this.height;
			}
		}
		if (this.pos.x < 0) {
			this.pos.x = 0;
		}
		if (this.pos.x > width-this.width) {
			this.pos.x = width-this.width;
		}
		this.velocity.x = 0;

	}

	collides(platform) {
		var centerX = this.pos.x + this.width/2;
		var bottomY = this.pos.y + this.height;
		var platformX = platform.pos.x;
		var platformY = platform.pos.y;

		if (centerX >= platformX && centerX <= platformX+platform.width) {
			if  (bottomY >= platformY && bottomY <= platformY + platform.height) {
				return true;
			}
		}
		return false;
	}

	move(vel) {
		this.velocity.x -= vel;
	}
	jump(vel) {
		this.velocity.y -= vel;
	}
}
