class Platform {
	constructor(ground) {
		if (ground) {
			this.pos = createVector(0, height-60);
			this.width = width;
			this.height = 60;
			this.velocity = 0;
		} else {
			var randomHeight = random(200, height-150);
			this.pos = createVector(width, randomHeight);
			this.width = 36 * 5;
			this.height = 24;
			this.velocity = -6;
		}
	}

	show() {
		if (this.velocity == 0) {
			fill(161, 80, 8); //Ground colour
		} else {
			fill(0); //Platform colour
		}
		rect(this.pos.x, this.pos.y, this.width, this.height);
	}

	update() {
		this.pos.x += this.velocity;
	}

	offScreen() {
		if (this.pos.x < -this.width) {
			return true;
		}
		return false;
	}
}
