let x, y;
let xspeed, yspeed;
let dvd;
let r, g, b;
function preload() {
	dvd = loadImage("dvd_logo.png");
}

function pickColour() {
	r = random(100, 256);
	g = random(100, 256);
	b = random(100, 256);
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	x = random(width);
	y = random(height);
	xspeed = 10;
	yspeed = 10;
	pickColour();
}

function draw() {
	background(0);
	// rect(x, y, 80, 60);
	
	tint(r, g, b);
	image(dvd, x, y);
	x += xspeed;
	y += yspeed;
	if (x + dvd.width >= width) {
		xspeed = -xspeed;	
		x = width - dvd.width;
		pickColour();
	} else if (x <= 0) {
		xspeed = -xspeed;	
		x = 0;
		pickColour();
	}

	if (y + dvd.height >= height ) {
		yspeed = -yspeed;	
		y = height - dvd.height;
		pickColour();
	} else if (y <= 0) {
		yspeed = -yspeed;	
		y = 0;
		pickColour();
	}
}
