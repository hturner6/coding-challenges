var cities = [];
var totalCities = 25;
var popSize = 1000;
var population = [];
var fitness = [];

var recordDistance = Infinity;
var bestRoute;
var currentBest;

var statusP;

function setup() {
	createCanvas(800, 800);
	var order =[];
	//Add cities to array
	for (var i = 0; i < totalCities; i++) {
		cities[i] = createVector(random(width), random(height/2));
		order[i] = i;
	}
	//Add several orders to population
	for (var i = 0; i < popSize; i++) {
		population[i] = shuffle(order);
	}

	statusP = createP('').style('font-size', '32pt');
}

function draw() {
	background(0);

	//GA
	calculateFitness();
	normaliseFitness();
	nextGeneration();
	//Draw current best route
	fill(255);
	beginShape();
	stroke(0, 255, 0);
	strokeWeight(4);
	noFill();
	for (var i = 0; i < bestRoute.length; i++) {
		var n = bestRoute[i];
		vertex(cities[n].x, cities[n].y);
		ellipse(cities[i].x, cities[i].y, 16, 16);
	}
	endShape();
	//Draw best route of generation
	translate(0, height/2);
	stroke(0, 255, 0);
	strokeWeight(2);
	noFill();
	beginShape();
	for (var i = 0; i < currentBest.length; i++) {
		var n = currentBest[i];
		vertex(cities[n].x, cities[n].y);
		ellipse(cities[i].x, cities[i].y, 16, 16);
	}
	endShape();
}

function swap(arr, i, j) {
	var temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

function calcDistance(points, order) {
	var sum = 0;
	for (var i = 0; i < order.length-1; i++) {
		var cityAIndex = order[i];
		var cityA = points[cityAIndex];
		var cityBIndex = order[i+1];
		var cityB = points[cityBIndex];
		sum += dist(cityA.x, cityA.y, cityB.x, cityB.y);
	}
	return sum;
}
