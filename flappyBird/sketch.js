var bird;
var pipes = [];
var pipeFrame = 100;
function setup() {
	createCanvas(400, 600);
	bird = new Bird();
	pipes.push(new Pipe());
}

function draw(){
	background(51);
	for (var i = pipes.length-1; i >= 0; i--) {
		pipes[i].show();
		pipes[i].update();
		if (pipes[i].hits(bird)) {
		}
		if (pipes[i].offScreen()) {
			pipes.splice(i, 1);
		}
	}
	bird.update();
	bird.show();

	if (frameCount % pipeFrame == 0) {
		pipes.push(new Pipe());
	}
}

function keyPressed() {
	if (key == ' ') {
		bird.jump();
	}
}
