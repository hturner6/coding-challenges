function Pipe() {
	var spacing = random(75, 150);
	var center = random(spacing, height-spacing);
	this.top = center - spacing/2;
	this.bottom = height - (center + spacing/2);
	this.x = width;
	this.width = 20;
	this.speed = 3;
	this.highlight = false;

	this.show = function() {
		if (this.highlight) {
			fill(255, 0, 0);
		} else {
			fill(150, 255, 150);
		}
		rect(this.x, 0, this.width, this.top);
		rect(this.x, height-this.bottom, this.width, this.bottom);
	}

	this.update = function() {
		this.x -= this.speed;
	}

	this.offScreen = function() {
		if (this.x < -this.width){
			return true;
		} 
		return false;
	}

	this.hits = function(bird) {
		if (bird.y < this.top || bird.y > height - this.bottom){
			if (bird.x > this.x && bird.x < this.x + this.width) {
				this.highlight = true;
				return true;
			}
		}
		this.highlight = false;
		return false;
	}
}
