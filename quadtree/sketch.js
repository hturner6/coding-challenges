var qtree;

function setup() {
	createCanvas(400, 400);
	background(255);
	let boundary = new Rectangle(200, 200, 200, 200);
	qtree = new QuadTree(boundary, 4);

	// Add points using random gaussian distribution
	for (var i = 0, len = 500; i < len; i++) {
		let x = randomGaussian(width/2, width/8);
		let y = randomGaussian(height/2, height/8);
		let p = new Point(x, y);
		qtree.insert(p);
	}

	// Add random points
	// for (var i = 0, noOfPoints = 50; i < noOfPoints; i++) {
	// 	let p = new Point(random(width), random(height));
	// 	qtree.insert(p);
	// }
	
	background(0);
	qtree.show();

	stroke(0, 255, 0);
	rectMode(CENTER);
	strokeWeight(2);
	let range = new Rectangle(random(width), random(height), 50, 50);
	rect(range.x, range.y, range.w*2, range.h*2);
	let points = qtree.query(range);
	console.log(points);
	for (let p of points) {
		strokeWeight(4);
		point(p.x, p.y);
	}
}

// function draw() {
// 	if (mouseIsPressed) {
// 		let m = new Point(mouseX, mouseY);
// 		qtree.insert(m);
// 	}
// 	background(0);
// 	qtree.show();
// }
