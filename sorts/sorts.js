let values = [];
let i = 0;
let j = 0;
function setup(){
	createCanvas(windowWidth, windowHeight);
	values = new Array(width);
	for(let i = 0; i < values.length;i++){
		values[i] = random(height);
		//values[i] = noise(i/100.0)*height;
	}
	//quickSort(values, 0, values.length-1);
}

function draw(){
	background(51);
	//quickSort(0, values.length-1);
	bubbleSort();
	showLines();
}

function showLines(){
	background(51);
	for(let i = 0; i < values.length; i++){
		stroke(255);
		line(i, height, i, height - values[i]);
	}
	console.log("updating lines");
	
}

function swap(a, b){
	let temp = values[a];
	values[a] = values[b];
	values[b] = temp;
}
