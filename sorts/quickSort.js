function quickSort(low, high){
	if(low<high){
		i+=1;
		p = partition2(low, high);
		quickSort(low, p-1);
		quickSort(p+1, high);
		i-=1;
	}
	if(i==0){
		noLoop();
		console.log("quick sort complete");
	}
}

function partition(low, high){
	pivot = values[low];
	i = low-1;
	j = high+1;
	while(true){
		do{
			i+=1;
		}while(values[i] < pivot){
		}
		do{
			j-=1;
		}
		while(values[j] > pivot){
		}
		if(i >= j){
			return j;
		}
		swap(i, j);
	}
}

function partition2(low, high){
	let pivot = values[high];
	let i = low;
	for(let j = low; j < high; j++){
		if(values[j] < pivot){
			if(i != j){
				swap(i, j);
			}
			i+=1;
		}
	}
	swap(i, high);
	return i;
}
