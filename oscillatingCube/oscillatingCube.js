let angle = 0;
let rectWidth = 24;
let maxD;
let ma;
function setup() {
  createCanvas(400, 400, WEBGL);
  background(0);
	ma = atan(cos(QUARTER_PI));
	maxD = dist(0, 0, 200, 200);

}

function draw() {
	background(175);
	ortho(-400, 400, 400, -400, 0, 1000);
	rotateX(-ma);
	rotateY(-QUARTER_PI);

	for(let j = 0; j < height; j+=rectWidth){
		for(let i = 0; i < width; i+=rectWidth){
			push();
			let d = dist(i, j, width/2, height/2);
			let offset = map(d, 0, maxD, -PI, PI);
			let a = angle + offset;
			let rectHeight =floor( map(sin(a), -1, 1, 100, 400));
			translate(i - width/2, 0 ,j - height/2);
			normalMaterial();
			box(rectWidth, rectHeight, rectWidth);
			pop();
		}
	}
	angle += 0.1;
}

