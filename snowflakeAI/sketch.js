let model;
let strokePath = null;

let x, y;
let pen = "down";
function setup() {
	createCanvas(windowWidth,windowHeight);
	x = random(-width/2, width/2);
	y = random(-height/2, height/2);
	model = ml5.SketchRNN("crab<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Snowflake SketchRNN</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/p5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/addons/p5.dom.min.js"></script>
    <script src="https://unpkg.com/ml5@0.1.3/dist/ml5.min.js"></script>
    <script src="sketch.js"></script>

    <style>
      html,
      body {
        margin: 0;
        padding: 0;
      }
      canvas {
        display: block;
      }
    </style>
  </head>
  <body></body>
</html>", modelReady);
	background(0);
}

function draw() {
	translate(width/2, height/2);
	if (strokePath != null){
		let newX = x + strokePath.dx*0.2;
		let newY = y + strokePath.dy*0.2;
		if (pen == "down"){
			stroke(255);
			strokeWeight(2);
			line(x, y, newX, newY);
		}
		pen = strokePath.pen;
		strokePath = null;
		x = newX;
		y = newY;
		if (pen != "end") {
			model.generate(gotSketch);
		} else {
			console.log("Drawing complete");
			model.reset();
			model.generate(gotSketch);
			x = random(-width/2, width/2);
			y = random(-height/2, height/2);
		}
	}
}

function modelReady(){
	console.log("Model Ready");
	model.reset();
	model.generate(gotSketch);
}

function gotSketch(error, s){
	if(error) {
		console.error(error);
	} else {
	strokePath = s;
	}
}
