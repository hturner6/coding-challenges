var slider;
var angle = 0;
var root;
var tree = [];
var leaves = [];
var count = 0;
function setup() {
	createCanvas(400, 400);
	slider = createSlider(0, TWO_PI, PI/8, 0.1);

	var a = createVector(width/2, height);
	var b = createVector(width/2, height-100);
	root = new Branch(a, b);

	tree[0] = root;
}

function mousePressed() {
	for (var i = tree.length-1; i >= 0; i--) {
		if(tree[i].finished == false) {
			tree.push(tree[i].branch(angle));
			tree.push(tree[i].branch(-angle));
			tree[i].finished = true;
		}
	}
	count++;

	if (count == 5) {
		for (var i = 0; i < tree.length; i++){
			if (!tree[i].finished) {
				var leaf = tree[i].end.copy();
				leaves.push(leaf);
			}
		}
	}
}

function draw() {
	background(51);
	angle = slider.value();
	
	for (var i = 0; i < tree.length; i++) {
		tree[i].show();
		//tree[i].jitter();
	}

	for (var i = 0; i < leaves.length; i++) {
		fill(255, 0, 255);
		ellipse(leaves[i].x, leaves[i].y, 8, 8);
	}
	
}

