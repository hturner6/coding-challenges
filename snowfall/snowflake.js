class Snowflake {
	
	constructor(sx, sy, img) {
		let x = sx || random(width);
		let y = sy || random(-100, -10);
		this.img = img;
		this.pos = createVector(x, y);
		this.vel = createVector(0, 0);
		this.acc = createVector();
		this.angle = random(TWO_PI);
		this.angleDir = (random(1) > 0.5) ? 1 : -1;
		this.xOffset = 0;
		this.r = getRandomSize();
	}

	applyForce(force) {
		let f = force.copy();		
		f.mult(this.r);
		this.acc.add(f);
	}

	offScreen() {
		return (this.pos.y > height + this.r);
	}

	update() {

		this.xOffset = sin(this.angle*2) * this.r;
		this.vel.add(this.acc);
		this.vel.limit(this.r * 0.2);
		if (this.vel.mag() < 1) {
			this.vel.normalize();	
		}
		this.pos.add(this.vel);
		this.acc.mult(0);

		if (this.pos.y > height + this.r) {
			this.randomise();
		}

		// Check for wrapping left and right
		this.wrap();
		this.angle += this.angleDir * this.vel.mag() / 200;
	}

	wrap() {
		if (this.pos.x < -this.r) {
			this.pos.x = width + this.r;
		}

		if (this.pos.x > width + this.r) {
			this.pos.x = -this.r;	
		}
	}

	render() {
		// Draw as point
		// stroke(255);
		// strokeWeight(this.r);
		// point(this.pos.x, this.pos.y);
		
		// Draw as snowflake img
		push();
		translate(this.pos.x + this.xOffset, this.pos.y);
		rotate(this.angle);
		imageMode(CENTER);
		image(this.img, 0, 0, this.r, this.r);
		pop();

	}

	randomise() {
		let x = random(width);
		let y = random(-100, -10);
		this.pos = createVector(x, y);
		this.vel = createVector(0, 0);
		this.acc = createVector();
		this.r = getRandomSize();
	}
}

function getRandomSize() {
	// using exponants
	let r = pow(random(0, 1), 3);
	return constrain(r * 36, 2, 32);

	
	// using Gaussian numbers
	// let r = randomGaussian() * 2;
	// return constrain(abs(r*r), 2, 36);

	// Using probability numbers
	// while (true) {
	// 	let r1 = random(1);
	// 	let r2 = random(2);
	// 	if (r2 > r1) {
	// 		return r1 * 16;
	// 	}
	// }
}
