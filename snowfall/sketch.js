let snow = [];
let gravity;
let spritesheet;
let textures = [];
let zOffset = 0;

function preload() {
	spritesheet = loadImage('flakes32.png');
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	gravity = createVector(0, 0.1);

	// Turn spritesheet into array of textures
	for (var i = 0, len = spritesheet.width; i < len; i += 32) {
		for (var j = 0, len = spritesheet.height; j < len; j += 32) {
			let img = spritesheet.get(i, j, 32, 32);
			textures.push(img);
		}
	}

	// Add snowflake objects to array  
	for (var i = 0, len = 600; i < len; i++) {
		let x = random(width);
		let y = random(height);
		let texture = random(textures);
		snow.push(new Snowflake(x, y, texture));
	}
}

function draw() {
	background(0);
	// Apply snowflake logic
	zOffset += 0.01;
	for (flake of snow) {
		let xOffset = flake.pos.x / width;
		let yOffset = flake.pos.y / height;
		let wAngle = noise(xOffset, yOffset, zOffset) * TWO_PI;
		let wind = p5.Vector.fromAngle(wAngle);
		wind.mult(0.1);

		flake.applyForce(gravity);
		flake.applyForce(wind);
		flake.render();
		flake.update();
	}

	// Delete snowflake if offscreen
	// for (let i = snow.length - 1; i >= 0; i--) {
	// 	if (snow[i].offScreen()) {
	// 		snow.splice(i, 1);
	// 	}
	// }
}
