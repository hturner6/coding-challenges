var blob;
var blobs = [];
var zoom = 1;
function setup(){
	createCanvas(800, 800);
	blob = new Blob(0, 0, 16, 255);
	console.log(blob.r);
	for (var i = 0, len = 1000; i < len; i++) {
		blobs[i] = new Blob(random(-width*2, width*2), random(-height*2, height*2), 8);
		
	}
}

function draw(){
	background(51);

	translate(width/2, height/2);
	var newzoom = 64 / blob.r;
	zoom = lerp(zoom, newzoom, 0.1);
	scale(zoom);
	translate(-blob.pos.x, -blob.pos.y);
	
	for (var i = blobs.length-1; i >= 0; i--) {
		blobs[i].show();
		if (blob.eats(blobs[i])) {
			blobs.splice(i , 1);
		}
	}

	blob.show();
	blob.update();
}

