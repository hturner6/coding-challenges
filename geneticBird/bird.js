function Bird(brain) {
	this.y = height/2;
	this.x = 64;

	this.gravity = 0.8;
	this.lift = -12;
	this.velocity = 0;
	
	this.score = 0;
	this.fitness = 0;
	if (brain) {
		this.brain = brain.copy();
	} else {
		this.brain = new NeuralNetwork(5, 8, 2);
	}

	this.show = function() {
		fill(255);
		noStroke();
		ellipse(this.x, this.y, 32, 32);
	}

	this.update = function() {
		this.score += 10;
		this.velocity += this.gravity;
		//this.velocity *= 0.9;
		this.y += this.velocity;

		if (this.y < 0) {
			this.y = 0;
			this.velocity = 0;
		}
	}

	this.mutate = function(probability) {
		this.brain.mutate(probability);
	}

	this.jump = function() {
		this.velocity += this.lift;
	}

	this.offScreen = function() {
		return (this.y > height);
	}
	this.think = function(pipes) {
		//find closest pipe
		let closest = null;
		let closestD = Infinity;
		for (var i = 0; i < pipes.length; i++) {
			let d = (pipes[i].x + pipes[i].width) - this.x;
			if (d < closestD && d > 0) {
				closest = pipes[i];
				closestD = d;
			}
		}

		let inputs = [];
		inputs[0] = this.y / height;
		inputs[1] = closest.top / height;
		inputs[2] = closest.bottom / height;
		inputs[3] = closest.x / width;
		inputs[4] = this.velocity / 10;
		let output = this.brain.predict(inputs);
		if (output[0] > output[1]) {
			this.jump();
		}
	}
}
