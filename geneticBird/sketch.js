const TOTAL = 1000;
var birds = [];
var pipes = [];
var savedBirds = [];
var counter = 0;
var currentScore = 0;
var slider;
function setup() {
	createCanvas(640, 480);
	slider = createSlider(1, 50, 1);
	for (var i = 0; i < TOTAL; i++) {
		birds[i] = new Bird();

	}
}

function draw(){
	for (let n = 0; n < slider.value(); n++) {
		if (counter % 100 == 0) { //add pipes every # of frames
			pipes.push(new Pipe());
		}
		counter++;

		for (var i = pipes.length-1; i >= 0; i--) { //pipe logic
			pipes[i].update();
			for (var j = birds.length-1; j >= 0; j--) {
				if (pipes[i].hits(birds[j])) {
					savedBirds.push(birds.splice(j, 1)[0]); //janky ass line
				}
			}
			if (pipes[i].offScreen()) {
				pipes.splice(i, 1);
			}
		}
		for (var i = birds.length-1; i >= 0; i--) {
			if (birds[i].offScreen()) {
				savedBirds.push(birds.splice(i, 1)[0]); //janky ass line
			}
		}

		for (let bird of birds) {
			bird.think(pipes);
			bird.update();
		}

		if (birds.length == 0) { //All birds die
			counter = 0;
			currentScore = 0;
			nextGeneration();
			pipes = [];
		}
		currentScore += 10;
	}


	background(51);
	for (let bird of birds) {
		bird.show();
	}
	for (let pipe of pipes) {
		pipe.show();
	}
	fill(255, 0, 0);
	textSize(32);
	text(currentScore, 500, 400);

}

//function keyPressed() {
//	if (key == ' ') {
//		bird.jump();
//	}
//}
