function Pipe() {

	//var spacing = random(100, 150);
	//this.top = random(height /6, 3 / 4 * height);
	//this.bottom = height - (this.top + spacing);

	var spacing = random(100, 175);
	var centering = random(spacing, height - spacing);
	this.top = centering - spacing/2;
	this.bottom = height - (centering + spacing/2);
	this.x = width;
	this.width = 80;
	this.speed = 6;

	this.highlight = false;

	this.show = function() {
		if (this.highlight) {
			fill(255, 0, 0);
		} else {
			fill(150, 255, 150);
		}
		rectMode(CORNER);
		rect(this.x, 0, this.width, this.top);
		rect(this.x, height-this.bottom, this.width, this.bottom);
	}

	this.update = function() {
		this.x -= this.speed;
	}

	this.offScreen = function() {
		if (this.x < -this.width){
			return true;
		} 
		return false;
	}

	this.hits = function(bird) {
		if (bird.y < this.top || bird.y > height - this.bottom){
			if (bird.x > this.x && bird.x < this.x + this.width) {
				this.highlight = true;
				return true;
			}
		}
		this.highlight = false;
		return false;
	}
}
