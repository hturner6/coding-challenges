let x , y;
let ax, ay;
let bx, by;
let cx, cy;
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  ax = random(width);
  ay = random(height);
  bx = random(width);
  by = random(height);
  cx = random(width);
  cy = random(height);
  x = random(width)
  y = random(height)
  stroke(255);
  strokeWeight(8);
  point(ax, ay);
  point(bx, by);
  point(cx, cy);
}

function draw() {
  for(i = 0; i < 100; i ++){
    stroke(255,0,255);
    strokeWeight(2);
    point(x, y);
    changePoint();
  }
}

function changePoint(){
   pointChoice = floor(random(3));
  
  if(pointChoice == 0){
    x = lerp(x, ax, 0.5);
    y = lerp(y, ay, 0.5);
  } else if(pointChoice == 1){
    x = lerp(x, bx, 0.5);
    y = lerp(y, by, 0.5);
  } else if(pointChoice == 2){
    x = lerp(x, cx, 0.5);
    y = lerp(y, cy, 0.5);
  }
}
